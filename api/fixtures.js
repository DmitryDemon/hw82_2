const mongoose = require('mongoose');
const config = require('./config');

const Artist = require('./models/Artist');
const Album = require('./models/Album');
const Track = require('./models/Track');

const run = async () => {
    await mongoose.connect(config.dbUrl, config.mongoOptions);

    const connection = mongoose.connection;

    const collections = await connection.db.collections();

    for (let collection of collections) {
        await collection.drop();
    }

    const artist = await Artist.create(
        {
            name: 'System of a Down',
            image:'soad.jpeg',
            description: 'American rock band formed in Los Angeles in 1992 ' +
                'by Serge Tankian and Daron Malakian called Soil,' +
                ' and in 1995 it adopted the current name.',
        },
        {
            name: 'The Doors',
            image:'the_doors.jpeg',
            description: 'The Doors were an American rock band formed in Los Angeles in 1965,' +
                ' with vocalist Jim Morrison, keyboardist Ray Manzarek, guitarist Robby Krieger,' +
                ' and drummer John Densmore. ',
        },
        {
            name: 'Shantel',
            image:'shantel.jpeg',
            description: 'Stefan Hantel is a German musician, producer and DJ,' +
                ' famous for electronic remixes of Balkan traditional music and his work with gypsy brass bands.'
        },
    );

    const album = await Album.create(
        {
            title: 'Toxicity',
            artist: artist[0]._id,
            albumYear: 2001,
            image: 'toxicity.jpeg'
        },
        {
            title: 'Mezmerize',
            artist: artist[0]._id,
            albumYear: 2005,
            image: 'mezmerize.jpeg'
        },
        {
            title: 'Strange Days',
            artist: artist[1]._id,
            albumYear: 1967,
            image: 'strange_days.jpeg'
        },
        {
            title: 'Disko Partizani',
            artist: artist[2]._id,
            albumYear: 2003,
            image: 'partizani.jpeg'
        },
    );
    await Track.create(
        {
            title: 'Chop Suey!',
            album: album[0]._id,
            duration: 221,
        },
        {
            title: 'Aerials',
            album: album[0]._id,
            duration: 371,
        },
        {
            title: 'Prison Song',
            album: album[0]._id,
            duration: 201,
        },
        {
            title: 'Radio/Video',
            album: album[1]._id,
            duration: 221,
        },
        {
            title: 'Old School Hollywood',
            album: album[1]._id,
            duration: 180,
        },
        {
            title: 'People Are Strange',
            album: album[2]._id,
            duration: 132,
        },
        {
            title: 'Unhappy Girl',
            album: album[2]._id,
            duration: 120,
        },
        {
            title: 'Fige Ki Ase Me',
            album: album[3]._id,
            duration: 340,
        },
        {
            title: 'Donna Diaspora',
            album: album[3]._id,
            duration: 300,
        },
        {
            title: 'Immigrant Child',
            album: album[3]._id,
            duration: 332,
        },
);


    await connection.close();
};



run().catch(error => {
    console.log('Something went wrong', error);
});