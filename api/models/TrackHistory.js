const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const TrackHistorySchema = new Schema({
    user: {//ID пользователя, прослушавшего композицию
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    },
    track: {//ID прослушанной композиции
        type: Schema.Types.ObjectId,
        ref: 'Track',
        required: true,
    },
    datetime: {//дата и время, когда композиция была прослушана
        type: String,
    }
});




const TrackHistory = mongoose.model('TrackHistory', TrackHistorySchema);

module.exports = TrackHistory;